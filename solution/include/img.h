#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct pixel { uint8_t color[3]; } pixel;

struct img
{
    uint64_t width, height;
    struct pixel* data;
};

const struct pixel * const_img_pixel_at(const struct img * image, uint64_t x, uint64_t y);

struct img new_img(uint64_t width, uint64_t height);
void delete_img(struct img image);
