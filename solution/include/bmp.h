#include "img.h"
#include <stdio.h>

enum read_status  {
    READ_OK,
    READ_STEAM_ERROR,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};
enum  write_status  {
    WRITE_OK,
    WRITE_STREAM_ERROR
};

enum read_status from_bmp(FILE* in_file, struct img* Image );
enum write_status to_bmp(FILE* out_file, const struct img* Image );
