#include "../include/img.h"
#include "../include/rotator.h"
#include <stdlib.h>

struct img rotate(struct img const source ) {

    struct img out = new_img(source.height, source.width);

    for (uint64_t x = 0; x < source.width; x++) {
        for (uint64_t y = 0; y < source.height; y++) {
            out.data[x * out.width + y] =  source.data[(source.height - 1 - y) * source.width + x];
        }
    }
    return out;

}
