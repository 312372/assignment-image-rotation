#include "../include/bmp.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

#define BTYPE 0x4D42
#define BSIZE 40
#define BPL 1
#define BBC 24
#define BFB 54

typedef struct bmp_header bmp_header;
static size_t bmp_header_size = sizeof( bmp_header);
static size_t pixel_size = sizeof(pixel);

static bmp_header getNewBmp(const struct img* img){
    struct bmp_header header = {
            .bfType = BTYPE,
            .bfileSize = (sizeof(struct bmp_header)
                          + img->height* img->width * sizeof(struct pixel)
                          + img->height* ((img->width)%4)),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BSIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BPL,
            .biBitCount = BBC,
            .biCompression = 0,
            .biSizeImage = img->height * img->width * sizeof(struct pixel) + (img->width % 4)*img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0

    };
    return header;
}



enum read_status from_bmp(FILE* in_file, struct img* image ){
    struct bmp_header* header = malloc(bmp_header_size);

    if (fread(header, bmp_header_size, 1, in_file) != 1) {
        return READ_STEAM_ERROR;
    }

    if(header->bOffBits != BFB || header->biSize != BSIZE)
        return READ_INVALID_HEADER;
    else if(header->biBitCount != BBC)
        return READ_INVALID_BITS;

    *image = new_img(header->biWidth, header->biHeight);

    for(uint32_t i = 0; i < header->biHeight; i++) {
        if (fread(&(image->data[i * image->width]), pixel_size, header->biWidth, in_file) != header->biWidth) {
            delete_img(*image);
            return READ_STEAM_ERROR;
        }
        if (fseek(in_file, header->biWidth % 4, SEEK_CUR)) {
            delete_img(*image);
            return READ_STEAM_ERROR;
        }
    }

    free(header);
    return READ_OK;
}


enum write_status to_bmp(FILE* out_file, const struct img* image ) {
    bmp_header bmp_header = getNewBmp(image);

    if (fwrite(&bmp_header, bmp_header_size, 1, out_file) != 1) {
        return WRITE_STREAM_ERROR;
    }

    const size_t zero = 0;
    for(uint32_t height=0; height < image->height; height++) {
        if (fwrite(const_img_pixel_at(image, 0, height), pixel_size, image->width, out_file) != image->width) {
            return WRITE_STREAM_ERROR;
        }
        if (fwrite(&zero, 1, image->width % 4, out_file) != (image->width % 4)) {
            return WRITE_STREAM_ERROR;
        }
    }
    return WRITE_OK;
}
