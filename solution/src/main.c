#include "../include/bmp.h"
#include "../include/rotator.h"
#include <stdlib.h>

int img_from_file(struct img * image, const char * path) {
    FILE *file = fopen(path, "r");
    if (file){
        if(from_bmp(file, image)) {
            printf("Input file is corrupted.");
            fclose(file);
            return (-3);
        }
        fclose(file);
        return 0;
    } else {
        printf("Input file was not open.");
        return (-3);
    }
}

int img_rotate(struct img * image) {
    struct img original_image = *image;
    struct img rotated_image = rotate(original_image);
    delete_img(original_image);
    *image = rotated_image;
    return 0;
}

int img_to_file(const struct img * image, const char * path) {
    FILE *file = fopen(path, "w");
    if (file) {
        if (to_bmp(file, image)) {
            printf("Output file is corrupted.");
            fclose(file);
            return (-3);
        }
        fclose(file);
        return 0;
    } else {
        printf("Output file was not open.");
        return (-3);
    }
}

int main( int argc, char** argv )
{
    if(argc >= 3)
    {
        struct img image;

        if (img_from_file(&image, argv[1])) {
            return (-3);
        }
        if (img_rotate(&image)) {
            delete_img(image);
            return (-3);
        }
        if (img_to_file(&image, argv[2])) {
            delete_img(image);
            return (-3);
        }
        delete_img(image);
    }
    else
    {
        printf("Not enough arguments.");
        return (-3);
    }
    return (0);
}
