#include "img.h"

const struct pixel * const_img_pixel_at(const struct img * image, uint64_t x, uint64_t y) {
    return &(image->data[y * image->width + x]);
}

struct img new_img(uint64_t width, uint64_t height) {
    struct pixel* const data = malloc(sizeof(pixel) * width * height);
    return (struct img){
        .height=height,
        .width=width,
        .data=data
    };
}

void delete_img(struct img image) {
    free(image.data);
}
